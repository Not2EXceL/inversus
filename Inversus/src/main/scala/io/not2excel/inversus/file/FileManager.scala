package io.not2excel.inversus.file

import java.io._

/*
 * Copyright (C) 2014-2015 Not2EXceL - Richmond Steele
 * **Totally didn't steal this from godshawk**
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
class FileManager() extends IOManager[BufferedReader, BufferedWriter]
{
    var file: File = _

    def setupNewStreams(file: File): Unit =
    {
        this.file = file
        closeStreams()
        setupReadStreams()
        setupWriteStreams()
    }

    override def setupReadStreams(): Unit =
    {
        if (this.readerOption.nonEmpty)
            this.readerOption = None
        try
        {
            this.readerOption = Some(new BufferedReader(new FileReader(file)))
        }
        catch
            {
                case e: IOException =>
                    println(s"There's an issue creating a BufferedReader for file ${file.getAbsolutePath}")
            }
    }

    override def setupWriteStreams(): Unit =
    {
        if (this.writerOption.nonEmpty)
            this.writerOption = None
        try
        {
            this.writerOption = Some(new BufferedWriter(new FileWriter(file, true)))
        }
        catch
            {
                case e: IOException =>
                    println(s"There's an issue creating a BufferedWriter for file ${file.getAbsolutePath}")
            }
    }

    def readLine(): String =
    {
        if (this.readerOption.nonEmpty)
        {
            try
            {
                return this.readerOption.get.readLine()
            }
            catch
                {
                    case e: IOException =>
                        e.printStackTrace()
                }
        }
        null
    }

    def readLines(num: Int): Array[String] =
    {
        val lines: Array[String] = new Array[String](num)
        for (i <- 0 to lines.length)
            lines(i) = readLine()
        lines
    }

    def readTillEnd(): Stream[String] =
    {
        Stream.continually(readLine()).takeWhile(_ != null)
    }

    def writeLine(line: String): Unit =
    {
        if (this.writerOption.nonEmpty)
        {
            try
            {
                this.writerOption.get.write(line)
            }
            catch
                {
                    case e: IOException =>
                        e.printStackTrace()
                }
        }
    }

    def writeLines(lines: String*): Unit =
    {
        for (line <- lines)
            writeLine(line)
    }

    def getFile: File = this.file

    def setFile(file: File): Unit = this.file = file
}
