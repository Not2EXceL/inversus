package io.not2excel.inversus.file

import java.io._

/*
 * Copyright (C) 2014-2015 Not2EXceL - Richmond Steele
 * **Totally didn't steal this from godshawk**
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
trait IOManager[R <: Reader, W <: Writer]
{
    var readerOption: Option[R] = None
    var writerOption: Option[W] = None

    def getReader: R = readerOption.get

    def getWriter: W = writerOption.get

    def setupReadStreams(): Unit

    def setupWriteStreams(): Unit

    def closeStreams(): Unit =
    {
        if (readerOption.nonEmpty)
        {
            try
            {
                readerOption.get.close()
            }
            catch
                {
                    case e: IOException => println("Error closing the read stream.")
                }
            finally
            {
                readerOption = None
            }
        }
        if (writerOption.nonEmpty)
        {
            try
            {
                writerOption.get.close()
            }
            catch
                {
                    case e: IOException => println("Error closing the writer stream.")
                }
            finally
            {
                writerOption = None
            }
        }
    }
}
