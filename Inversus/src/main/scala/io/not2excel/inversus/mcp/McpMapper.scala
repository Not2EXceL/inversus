package io.not2excel.inversus.mcp

import java.io.File

import io.not2excel.inversus.file.FileManager
import io.not2excel.inversus.mcp.McpObject._

import scala.collection.mutable


/*
 * Copyright (C) 2014-2015 Not2EXceL - Richmond Steele
 * **Totally didn't steal this from godshawk**
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
object McpMapper
{
    val mcpFiles   : Array[File] = Array(new File(getClass.getResource("/mcpConfig/client.srg").toURI),
                                         new File(getClass.getResource("/mcpConfig/methods.csv").toURI),
                                         new File(getClass.getResource("/mcpConfig/fields.csv").toURI))
    val fileManager: FileManager = new FileManager()

    val csvFields : mutable.OpenHashMap[String, CsvMapping] = new mutable.OpenHashMap[String, CsvMapping]()
    val csvMethods: mutable.OpenHashMap[String, CsvMapping] = new mutable.OpenHashMap[String, CsvMapping]()
    val mcpClasses: mutable.OpenHashMap[String, McpClass]   = new mutable.OpenHashMap[String, McpClass]()
    val mcpMethods: mutable.OpenHashMap[String, McpMethod]  = new mutable.OpenHashMap[String, McpMethod]()
    val mcpFields : mutable.OpenHashMap[String, McpField]   = new mutable.OpenHashMap[String, McpField]()

    parseMappings()

    def parseMappings(): Unit =
    {
        println("Parsing client.srg")
        //joined srg
        fileManager.setupNewStreams(mcpFiles(0))
        parseJoined()
        println("Finished parsing client.srg")

        println("Parsing methods.csv...")
        //methods.csv
        fileManager.setupNewStreams(mcpFiles(1))
        parseMethods()
        println("Finished parsing methods.csv")

        println("Parsing fields.csv...")
        //fields.csv
        fileManager.setupNewStreams(mcpFiles(2))
        parseFields()
        println("Finished parsing fields.csv")

        fileManager.closeStreams()
    }

    private def parseJoined(): Unit =
    {

        fileManager.readTillEnd().foreach(line =>
                                          {
                                              val split: Array[String] = line.split("\\s+")
                                              split(0) match
                                              {
                                                  case "CL:" =>
                                                      val mapping: McpClass = new McpClass(split)
                                                      mcpClasses += (mapping.name -> mapping)
                                                  case "MD:" =>
                                                      val mapping: McpMethod = new McpMethod(split)
                                                      mcpMethods += (mapping.seargePath -> mapping)
                                                  case "FD:" =>
                                                      val mapping: McpField = new McpField(split)
                                                      mcpFields += (mapping.seargePath -> mapping)
                                                  case _ =>
                                              }
                                          })
    }

    private def parseMethods(): Unit =
    {
        fileManager.readTillEnd().foreach(line =>
                                          {
                                              val split: Array[String] = line.split(",")
                                              val mapping: CsvMapping = new CsvMapping(split)
                                              csvMethods += (mapping.name -> mapping)
                                          })
    }

    private def parseFields(): Unit =
    {
        fileManager.readTillEnd().foreach(line =>
                                          {
                                              val split: Array[String] = line.split(",")
                                              val mapping: CsvMapping = new CsvMapping(split)
                                              csvFields += (mapping.name -> mapping)
                                          })
    }

    def getClassOption(clazz: String): Option[McpClass] =
    {
        mcpClasses.get(constructClass(clazz))
    }

    def getMethodOption(clazz: String, method: String, sig: String): Option[McpMethod] =
    {
        val csvMapping: Option[CsvMapping] = csvMethods.get(method)
        println(constructPath(clazz, csvMapping.get.searge))
        if (csvMapping.nonEmpty)
        {
            val mapping: Option[McpMethod] = mcpMethods.get(constructPath(clazz, csvMapping.get.searge))
            if (mapping.nonEmpty)
                if (mapping.get.unObfSig.equalsIgnoreCase(sig))
                    mapping
                else
                    None
            else
                None
        }
        else
            None
    }

    def getFieldOption(clazz: String, field: String): Option[McpField] =
    {
        val mapping: Option[CsvMapping] = csvFields.get(field)
        if (mapping.nonEmpty)
            mcpFields.get(constructPath(clazz, mapping.get.searge))
        else
            None
    }

    private def constructPath(clazz: String, append: String): String =
    {
        constructClass(clazz) + "/" + append
    }

    private def constructClass(clazz: String): String =
    {
        if (clazz.contains("."))
        {
            clazz.replaceAll("\\.", "/")
        }
        else
            clazz
    }
}
