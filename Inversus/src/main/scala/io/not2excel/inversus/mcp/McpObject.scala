package io.not2excel.inversus.mcp

/**
 * Created by open on 11/13/2014.
 */
object McpObject
{

    class McpClass(srgSplit: Array[String])
    {
        val clientSRG: Array[String] = srgSplit
        val obf      : String        = srgSplit(1)
        val name     : String        = srgSplit(2)
    }

    class McpMethod(srgSplit: Array[String])
    {
        val clientSRG : Array[String] = srgSplit
        val obfPath   : String        = srgSplit(1)
        val obfSig    : String        = srgSplit(2)
        val seargePath: String        = srgSplit(3)
        val unObfSig  : String        = srgSplit(4)
        val obfName   : String        = obfPath.substring(obfPath.lastIndexOf("/") + 1)
        val seargeName: String        = seargePath.substring(seargePath.lastIndexOf("/") + 1)
        val obfClass  : String        = obfPath.substring(0, obfPath.lastIndexOf("/"))
        val unObfClass: String        = seargePath.substring(0, seargePath.lastIndexOf("/"))
    }

    class McpField(srgSplit: Array[String])
    {
        val clientSRG : Array[String] = srgSplit
        val obfPath   : String        = srgSplit(1)
        val seargePath: String        = srgSplit(2)
        val obfName   : String        = obfPath.substring(obfPath.lastIndexOf("/") + 1)
        val seargeName: String        = seargePath.substring(seargePath.lastIndexOf("/") + 1)
        val obfClass  : String        = obfPath.substring(0, obfPath.lastIndexOf("/"))
        val unObfClass: String        = seargePath.substring(0, seargePath.lastIndexOf("/"))
    }

    class CsvMapping(split: Array[String])
    {
        val csv: Array[String] = split
        val searge: String = split(0)
        val name  : String = split(1)
        val side  : String = split(2)
        var desc  : String = ""
        if (split.length == 4)
        {
            desc = split(3)
        }
    }

}
