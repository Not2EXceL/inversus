package io.not2excel.inversus.mcp.ui

import javafx.application.Application
import javafx.event.{ActionEvent, EventHandler}
import javafx.geometry.{Insets, Pos}
import javafx.scene.control._
import javafx.scene.layout.GridPane
import javafx.scene.paint.Color
import javafx.scene.shape.Rectangle
import javafx.scene.{Group, Scene}
import javafx.stage.Stage

import io.not2excel.inversus.mcp.McpObject.{McpField, McpMethod, McpClass}
import io.not2excel.inversus.mcp.McpType._
import io.not2excel.inversus.mcp.{McpMapper, McpType}

import scala.collection.mutable.ArrayBuffer

/*
 * Copyright (C) 2014-2015 Not2EXceL - Richmond Steele
 * **Totally didn't steal this from godshawk**
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
class McpViewer extends Application
{
    McpMapper

    override def start(primaryStage: Stage): Unit =
    {
        val width: Double = 400D
        val height: Double = 300D
        primaryStage.setTitle("MCP Viewer")
        val root: Group = new Group()

        val tabPane: TabPane = new TabPane()
        tabPane.setPrefSize(width, height)

        val mainTabGrid: GridPane = new GridPane()

        val mainTab: Tab = new Tab()
        mainTab.setText("MCP Search")
        mainTab.setContent(new Rectangle(width, height, Color.GREY))
        mainTab.setClosable(false)
        val mainGrid: GridPane = new GridPane()
        mainGrid.setAlignment(Pos.TOP_LEFT)
        mainGrid.setHgap(5D)
        mainGrid.setVgap(5D)
        mainGrid.setPadding(new Insets(10D, 0D, 10D, 10D))

        val choiceCombo: ComboBox[String] = new ComboBox[String]()
        choiceCombo.setPromptText("Select Type to search")
        choiceCombo.getItems.addAll("Class", "Method", "Field")

        val choiceButton: Button = new Button()
        choiceButton.setVisible(false)
        choiceButton.setText("Search MCP Mappings")
        choiceButton.setOnAction(new EventHandler[ActionEvent]
        {
            override def handle(event: ActionEvent): Unit =
            {
                if (!choiceCombo.getSelectionModel.isEmpty)
                {
                    val strings: ArrayBuffer[String] = new ArrayBuffer[String]()
                    for (i: Int <- 0 to mainTabGrid.getChildren.size() - 1)
                    {
                        mainTabGrid.getChildren.get(i) match
                        {
                            case field: TextField =>
                                strings += field.getText
                            case _ =>
                        }
                    }
                    val tab: Tab = generateTab(McpType.valueOf(choiceCombo.getSelectionModel
                                                                   .getSelectedItem.toUpperCase),
                                               strings)
                    tabPane.getTabs.add(tab)
                    tabPane.getSelectionModel.select(tab)
                    choiceCombo.setValue(null)
                    changeChoice(mainTabGrid, null)
                    choiceButton.setVisible(false)
                }
            }
        })

        choiceCombo.setOnAction(new EventHandler[ActionEvent]
        {
            override def handle(event: ActionEvent): Unit =
            {
                changeChoice(mainTabGrid, McpType.valueOf(choiceCombo.getSelectionModel.getSelectedItem.toUpperCase))
                choiceButton.setVisible(true)
            }
        })

        mainGrid.add(choiceCombo, 0, 0)
        mainGrid.add(choiceButton, 0, 2)
        mainGrid.add(mainTabGrid, 0, 1)
        mainTab.setContent(mainGrid)

        tabPane.getTabs.add(mainTab)
        root.getChildren.add(tabPane)
        val scene: Scene = new Scene(root, width, height)
        //        scene.getStylesheets.add(getClass.getResource("/theme.css").toExternalForm)
        primaryStage.setScene(scene)
        primaryStage.setResizable(false)
        primaryStage.show()
    }

    def changeChoice(grid: GridPane, mcpType: McpType): Unit =
    {
        grid.getChildren.clear()
        if (mcpType == null)
            return
        val cLabel: Label = new Label()
        cLabel.setText("Full Class name:")
        val cText: TextField = new TextField()
        grid.add(cLabel, 0, 0)
        grid.add(cText, 0, 1)
        val label: Label = new Label()
        val text: TextField = new TextField()
        mcpType match
        {
            case CLASS =>
            //none size the label already exists
            case METHOD =>
                label.setText("Method name:")
                grid.add(label, 0, 2)
                grid.add(text, 0, 3)
                grid.add(new Label("Signature:"), 0, 4)
                grid.add(new TextField(), 0, 5)
            case FIELD =>
                label.setText("Field name:")
                grid.add(label, 0, 2)
                grid.add(text, 0, 3)
            case _ =>
        }
    }

    def generateTab(mcpType: McpType, strings: ArrayBuffer[String]): Tab =
    {
        val tab: Tab = new Tab()
        val grid: GridPane = new GridPane()
        grid.setAlignment(Pos.TOP_LEFT)
        grid.setHgap(5D)
        grid.setVgap(5D)
        grid.setPadding(new Insets(10D, 0D, 10D, 10D))

        mcpType match
        {
            case CLASS =>
                if (strings.size < 1)
                {
                    grid.add(new Label("Failed to input a class."), 0, 0)
                    tab.setText("Failure")
                    tab.setContent(grid)
                    return tab
                }
                if (strings(0).equalsIgnoreCase(""))
                {
                    grid.add(new Label("Failed to input a class."), 0, 0)
                    tab.setText("Failure")
                    tab.setContent(grid)
                    return tab
                }
                val classOption: Option[McpClass] = McpMapper.getClassOption(strings(0))
                if(classOption.nonEmpty)
                {
                    tab.setText(strings(0))
                    grid.add(new Label("Unobfuscated:"), 0, 0)
                    grid.add(new Label(classOption.get.name), 0, 1)
                    grid.add(new Label("Obfuscated:"), 0, 2)
                    grid.add(new Label(classOption.get.obf), 0, 3)
                }
                else
                {
                    grid.add(new Label("Failed."), 0, 0)
                    tab.setText("Failure")
                    tab.setContent(grid)
                    return tab
                }
            case METHOD =>
                if (strings.size < 3)
                {
                    grid.add(new Label("Failed to input a class or method."), 0, 0)
                    tab.setText("Failure")
                    tab.setContent(grid)
                    return tab
                }
                if (strings(0).equalsIgnoreCase("") ||
                    strings(1).equalsIgnoreCase("") ||
                    strings(2).equalsIgnoreCase(""))
                {
                    grid.add(new Label("Failed to input a class or method or sig."), 0, 0)
                    tab.setText("Failure")
                    tab.setContent(grid)
                    return tab
                }
                val methodOption: Option[McpMethod] = McpMapper.getMethodOption(strings(0), strings(1), strings(2))
                if(methodOption.nonEmpty)
                {
                    tab.setText(strings(1))
                    grid.add(new Label("Unobfuscated Class:"), 0, 0)
                    grid.add(new Label(methodOption.get.unObfClass), 0, 1)
                    grid.add(new Label("Obfuscated Class:"), 0, 2)
                    grid.add(new Label(methodOption.get.obfClass), 0, 3)
                    grid.add(new Label("Unobfuscated Name:"), 0, 4)
                    grid.add(new Label(strings(1)), 0, 5)
                    grid.add(new Label("Obfuscated Name:"), 0, 6)
                    grid.add(new Label(methodOption.get.obfName), 0, 7)
                    grid.add(new Label("Unobfuscated Signature:"), 0, 8)
                    grid.add(new Label(methodOption.get.unObfSig), 0, 9)
                    grid.add(new Label("Obfuscated Signature:"), 0, 10)
                    grid.add(new Label(methodOption.get.obfSig), 0, 11)
                }
                else
                {
                    grid.add(new Label("Failed."), 0, 0)
                    tab.setText("Failure")
                    tab.setContent(grid)
                    return tab
                }
            case FIELD =>
                if (strings.size < 2)
                {
                    grid.add(new Label("Failed to input a class or field."), 0, 0)
                    tab.setText("Failure")
                    tab.setContent(grid)
                    return tab
                }
                if (strings(0).equalsIgnoreCase("") ||
                    strings(1).equalsIgnoreCase(""))
                {
                    grid.add(new Label("Failed to input a class or field."), 0, 0)
                    tab.setText("Failure")
                    tab.setContent(grid)
                    return tab
                }
                val fieldOption: Option[McpField] = McpMapper.getFieldOption(strings(0), strings(1))
                if(fieldOption.nonEmpty)
                {
                    tab.setText(strings(1))
                    grid.add(new Label("Unobfuscated Class:"), 0, 0)
                    grid.add(new Label(fieldOption.get.unObfClass), 0, 1)
                    grid.add(new Label("Obfuscated Class:"), 0, 2)
                    grid.add(new Label(fieldOption.get.obfClass), 0, 3)
                    grid.add(new Label("Unobfuscated Name:"), 0, 4)
                    grid.add(new Label(strings(1)), 0, 5)
                    grid.add(new Label("Obfuscated Name:"), 0, 6)
                    grid.add(new Label(fieldOption.get.obfName), 0, 7)
                }
                else
                {
                    grid.add(new Label("Failed."), 0, 0)
                    tab.setText("Failure")
                    tab.setContent(grid)
                    return tab
                }
            case _ =>
        }

        tab.setContent(grid)
        tab
    }
}
